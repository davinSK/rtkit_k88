@shift /0
TITLE TREK MOD. alwayne@supportking.com

color 3e

cls

@echo off

adb kill-server

adb start-server

adb wait-for-device
echo Device Detected!

adb reboot edl
timeout /t 2 /nobreak >NUL

jasminetool-win64.exe write recovery jasmine-twrp-7.1-r3.img
timeout /t 33 /nobreak >NUL
adb kill-server

adb start-server

adb wait-for-device

adb reboot recovery
timeout /t 2 /nobreak >NUL

echo Wipe cache, dalvik and data then go back and flash zip files in their order
timeout /t 10 /nobreak >NUL

echo Profit!