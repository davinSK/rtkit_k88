﻿Imports System.IO
Imports System.Net
Imports System.Threading
Imports LibUsbDotNet
Imports LibUsbDotNet.Main
Imports SharpAdbClient

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim server = New AdbServer()
        Dim result = server.StartServer(Environment.CurrentDirectory & "\ADB\adb.exe", restartServerIfNewer:=False)
        'TODO:catch errors
        'TODO: check for external sd
        'Dim monitor = New DeviceMonitor(New AdbSocket(New IPEndPoint(IPAddress.Loopback, AdbClient.AdbServerPort)))
        'AddHandler monitor.DeviceConnected, AddressOf OnDeviceConnected
        'AddHandler monitor.DeviceDisconnected, AddressOf OnDeviceDisConnected
        'monitor.Start()
        'If AdbClient.Instance.GetDevices().Count > 0 Then
        '    Dim device = AdbClient.Instance.GetDevices.First
        '    connecteddevice_lbl.Text = device.Name & $" ({device.Model})"
        '    connectionindicator_pb.BackColor = Color.Green
        '    writetolog($"The device {device.Name} has been detected")
        'End If
        pop_steps_lb()
    End Sub
    Sub pop_steps_lb()
        steps_lb.Items.Clear()
        steps_lb.Items.Add("1) Perform initial tablet setup")
        steps_lb.Items.Add("2) Enable USB Debugging") ' TODO: grey out when device connected
        steps_lb.Items.Add("3) Connect Device to PC") ' TODO: grey out when device connected
        steps_lb.Items.Add("4) Transfer files to the device") 'TODO: open file explorer to my pc
        steps_lb.Items.Add("5) Reboot to Recovery")
        steps_lb.Items.Add("6) Install Android OS Update")
        steps_lb.Items.Add("7) Reboot to Emergency Download mode")
        steps_lb.Items.Add("8) Install WinUSB driver") 'TODO: check if winusb already installed, or leave a token after install for this app to see this step can be skipped
        steps_lb.Items.Add("9) Flash Recovery")
        steps_lb.Items.Add("10) Reboot to Recovery")
        steps_lb.Items.Add("11) Install ROM")
    End Sub
    Private Sub OnDeviceConnected(ByVal sender As Object, ByVal e As DeviceDataEventArgs)
        Console.WriteLine(e.Device.Model)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() OnDeviceConnected(sender, e))
            Return
        End If
        connecteddevice_lbl.Text = e.Device.Name & $" ({e.Device.Model})"
        connectionindicator_pb.BackColor = Color.Green
        writetolog($"The device {e.Device.Name} has connected to this PC")
    End Sub
    Private Sub OnDeviceDisConnected(ByVal sender As Object, ByVal e As DeviceDataEventArgs)
        If AdbClient.Instance.GetDevices().Count > 0 Then
            Return
        End If
        If Me.InvokeRequired Then
            Me.Invoke(Sub() OnDeviceDisConnected(sender, e))
            Return
        End If
        connecteddevice_lbl.Text = "No Device Detected"
        connectionindicator_pb.BackColor = Color.Black
        writetolog($"The device {e.Device.Name} has disconnected from this PC")
    End Sub
    Sub writetolog(thisstring As String)
        Console.WriteLine(thisstring)
        log_tb.AppendText(thisstring & vbNewLine)
    End Sub

    Private Sub steps_lb_SelectedValueChanged(sender As Object, e As EventArgs) Handles steps_lb.SelectedValueChanged
        For i = instructions_flp.Controls.Count - 1 To 0 Step -1
            If Not instructions_flp.Controls(i).Name = "instructions_tb" Then
                instructions_flp.Controls.Remove(instructions_flp.Controls(i))
            End If
        Next
        instructions_tb.Text = ""
        If (steps_lb.SelectedIndex <> -1) Then
            Select Case steps_lb.SelectedItem.ToString().Split(")")(0)
                Case "1"
                    instructions_tb.Text = "Navigate and skip through the initial setup wizard until you reach the home screen."
                Case "2"
                    instructions_tb.Text = $"You can enable USB Debugging by:{vbNewLine}-Go to Settings>About Tablet and tap Build Number until you see the message ""You are now a developer!""{vbNewLine}-Go back to settings, go to Developer Settings and enable USB Debugging and OEM Unlocking"
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "View Instructions Online"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   Process.Start("https://www.softwarert.com/enable-developer-options-android-6/")
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "3"
                    instructions_tb.Text = "When connecting, select MTP as the connection method. A popup will appear on the device titled ""Allow USB Debugging"", check the box to always allow connections to this PC."
                Case "4"
                    instructions_tb.Text = $"You will need to transfer the files to the root of the SD card on the device. The files needed are:{vbNewLine}-update.zip{vbNewLine}-VersionX.zip{vbNewLine}-SuperSU.zip{vbNewLine}-B15 Folder"
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Do it for me"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If AdbClient.Instance.GetDevices().Count > 0 Then
                                                       Dim filelist As List(Of String) = New List(Of String)
                                                       Dim result1 As DialogResult = OpenFileDialog1.ShowDialog()
                                                       If result1 = DialogResult.OK Then
                                                           filelist.Add(OpenFileDialog1.FileName)
                                                           writetolog($"Added {OpenFileDialog1.FileName}")
                                                       Else
                                                           writetolog($"File selection canceled")
                                                           Exit Sub
                                                       End If
                                                       'Dim result2 As DialogResult = OpenFileDialog1.ShowDialog()
                                                       'If result2 = DialogResult.OK Then
                                                       '    filelist.Add(OpenFileDialog1.FileName)
                                                       '    writetolog($"Added {OpenFileDialog1.FileName}")
                                                       'Else
                                                       '    writetolog($"File selection canceled")
                                                       '    Exit Sub
                                                       'End If
                                                       'Dim result3 As DialogResult = OpenFileDialog1.ShowDialog()
                                                       'If result3 = DialogResult.OK Then
                                                       '    filelist.Add(OpenFileDialog1.FileName)
                                                       '    writetolog($"Added {OpenFileDialog1.FileName}")
                                                       'Else
                                                       '    writetolog($"File selection canceled")
                                                       '    Exit Sub
                                                       'End If
                                                       Dim device = AdbClient.Instance.GetDevices.First
                                                       For Each filestring In filelist

                                                           Dim service As SyncService = New SyncService(New AdbSocket(New IPEndPoint(IPAddress.Loopback, AdbClient.AdbServerPort)), device)
                                                           Dim stream As Stream = File.OpenRead(filestring)
                                                           service.Push(stream, "/${SECONDARY_STORAGE%%:*}/" & System.IO.Path.GetFileName(filestring), 444, DateTime.Now, Nothing, CancellationToken.None)
                                                           writetolog($"successfully copied {filestring} to device")
                                                       Next
                                                       writetolog($"file copy complete")
                                                   Else
                                                       writetolog($"Failed transferring files, no device detected")
                                                   End If
                                               End Sub
                    'instructions_flp.Controls.Add(MyButton)
                Case "5"
                    instructions_tb.Text = $"You can reboot into recovery mode by:{vbNewLine}-Turn off device completely and unplug the USB{vbNewLine}-Hold down Volume Up and Power buttons{vbNewLine}-Once the screen turns on, release the Power button only{vbNewLine}-When the Recovery mode screen appears, release the Volume Up button"
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Reboot into Recovery"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If AdbClient.Instance.GetDevices().Count > 0 Then
                                                       Dim device = AdbClient.Instance.GetDevices.First
                                                       Dim receiver = New ConsoleOutputReceiver()
                                                       AdbClient.Instance.ExecuteRemoteCommand("reboot recovery", device, receiver)
                                                   Else
                                                       writetolog($"Failed rebooting to recovery, no device detected")
                                                   End If
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "6"
                    instructions_tb.Text = $"Install the updated Android OS by:{vbNewLine}-Use the Volume & Power keys to select Apply Update from SD Card{vbNewLine}-Select the Update.zip file and select it to apply the update{vbNewLine}-If update fails, select instead the Update.zip in the B15 folder{vbNewLine}-Once the update is complete, reboot the device. You will then need to again get through the initial setup wizard and Enable USB Debugging and OEM Unlocking"
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Reboot into Recovery"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If AdbClient.Instance.GetDevices().Count > 0 Then
                                                       Dim device = AdbClient.Instance.GetDevices.First
                                                       Dim receiver = New ConsoleOutputReceiver()
                                                       AdbClient.Instance.ExecuteRemoteCommand("reboot recovery", device, receiver)
                                                   Else
                                                       writetolog($"Failed rebooting to recovery, no device detected")
                                                   End If
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "7"
                    instructions_tb.Text = $"You can enter Emergency Download mode by:{vbNewLine}-Power off and unplug the device.{vbNewLine}-Hold both volume buttons.{vbNewLine}-Plug the device into your PC.{vbNewLine}Wait about 3 seconds and release the buttons. This should enter you to EDL mode.{vbNewLine}NOTE: The screen will be black and the device will appear powered off while in EDL mode"
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Reboot into EDL Mode"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If AdbClient.Instance.GetDevices().Count > 0 Then
                                                       Dim device = AdbClient.Instance.GetDevices.First
                                                       Dim receiver = New ConsoleOutputReceiver()
                                                       AdbClient.Instance.ExecuteRemoteCommand("reboot edl", device, receiver)
                                                   Else
                                                       writetolog($"Failed rebooting to EDL mode, no device detected")
                                                   End If
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "8"
                    instructions_tb.Text = $"To install WinUSB driver:{vbNewLine}-Open Zadig by clicking the button below or opening manually (while device in EDL mode){vbNewLine}-Find device with ID 05c6:9008 and install the WinUSB driver for it. If it doesn't appear, go to Options>List All Devices{vbNewLine}-When installed successfully, reboot device by holding down the Power Button until the screen turns back on."
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Open Zadig Utility"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   Process.Start(Environment.CurrentDirectory & "\zadig-2.3.exe")
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "9"
                    instructions_tb.Text = $"To Flash recovery via Jasmine tool (Manually):{vbNewLine}-Boot into EDL mode (you can use the button from step 7){vbNewLine}-Run via command line ""jasminetool-win64.exe write recovery jasmine-twrp-7.1-r3.img""{vbNewLine}-When Prompted, type ""I AGREE"" to allow the tool to proceed. It will reboot when complete."
                    'Dim MyButton2 As New Button()
                    'MyButton2.Name = "MyButton1"
                    'MyButton2.Text = "Open Jasmine"
                    'MyButton2.Width = instructions_flp.Width - 5
                    'MyButton2.Height = 20
                    'AddHandler MyButton2.Click, Sub(sender2, eventargs2)
                    '                                Dim proc = New Process
                    '                                Dim startinfo = New ProcessStartInfo
                    '                                startinfo.FileName = Environment.CurrentDirectory & "\Jasmine\jasminetool-win64.exe"
                    '                                startinfo.Arguments = $"write recovery {Environment.CurrentDirectory}\Jasmine\jasmine-twrp-7.1-r3.img"
                    '                                startinfo.UseShellExecute = True
                    '                                startinfo.RedirectStandardOutput = True
                    '                                startinfo.RedirectStandardError = True
                    '                                startinfo.RedirectStandardInput = True
                    '                                startinfo.CreateNoWindow = False
                    '                                proc.StartInfo = startinfo
                    '                                proc.Start()
                    '                                writetolog($"Opening Jasmine for manual run")
                    '                            End Sub
                    'instructions_flp.Controls.Add(MyButton2)
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton2"
                    MyButton.Text = "Do it for me"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 20
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If Not jasmine_BackgroundWorker1.IsBusy Then
                                                       Select Case connectionindicator_pb.BackColor
                                                           Case Color.Green
                                                               writetolog($"Rebooting to EDL mode")
                                                               Dim device = AdbClient.Instance.GetDevices.First
                                                               Dim receiver = New ConsoleOutputReceiver()
                                                               AdbClient.Instance.ExecuteRemoteCommand("reboot edl", device, receiver)
                                                           Case Color.Yellow

                                                           Case Else
                                                               MsgBox("Device not detected")
                                                               writetolog($"Device not detected")
                                                               Exit Sub
                                                       End Select
                                                       writetolog($"***STARTING JASMINE EXECUTION (after a 10 second delay)***")
                                                       jasmine_BackgroundWorker1.RunWorkerAsync()
                                                   Else
                                                       writetolog($"Jasime already running")
                                                   End If
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "10"
                    instructions_tb.Text = $"Please reboot into recovery. If TWRP asks for a password, just cancel and swipe to allow modifications."
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Reboot into TWRP Recovery"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If AdbClient.Instance.GetDevices().Count > 0 Then
                                                       Dim device = AdbClient.Instance.GetDevices.First
                                                       Dim receiver = New ConsoleOutputReceiver()
                                                       AdbClient.Instance.ExecuteRemoteCommand("reboot recovery", device, receiver)
                                                   Else
                                                       writetolog($"Failed rebooting to recovery, no device detected")
                                                   End If
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
                Case "11"
                    instructions_tb.Text = $"Follow the below process to manually install the custom ROM (or use the button below):{vbNewLine}Click on Wipe>Advanced Wipe, then select Cache, Dalvik and Data and perform the wipe{vbNewLine}-On the TWRP home screen, select Install and select VersionX.zip, then select Add More Zips and select SuperSU.zip. Swipe to flash.{vbNewLine}-Once it’s complete, tap reboot system and if it asks you if you want to install SuperSU or TWRP. Tap Do Not Install."
                    Dim MyButton As New Button()
                    MyButton.Name = "MyButton"
                    MyButton.Text = "Install ROM Automatically"
                    MyButton.Width = instructions_flp.Width - 5
                    MyButton.Height = 30
                    AddHandler MyButton.Click, Sub(sender2, eventargs2)
                                                   If AdbClient.Instance.GetDevices().Count > 0 Then
                                                       Dim input_vfile = InputBox("Please enter filename of the ""Version"" file on the device")
                                                       If Not input_vfile.ToLower().Contains(".zip") Then
                                                           MsgBox("Please input a filename ending in .zip")
                                                           Exit Sub
                                                       End If
                                                       Dim input_SUfile = InputBox("Please enter filename of the ""SuperSU"" file on the device",, "SuperSU.zip")
                                                       If Not input_SUfile.ToLower().Contains(".zip") Then
                                                           MsgBox("Please input a filename ending in .zip")
                                                           Exit Sub
                                                       End If

                                                       Dim device = AdbClient.Instance.GetDevices.First
                                                       Dim receiver = New ConsoleOutputReceiver()
                                                       writetolog($"Wiping Cache")
                                                       AdbClient.Instance.ExecuteRemoteCommand("twrp wipe cache", device, receiver)
                                                       writetolog($"Wiping Dalvik")
                                                       AdbClient.Instance.ExecuteRemoteCommand("twrp wipe dalvik", device, receiver)
                                                       writetolog($"Wiping Data")
                                                       AdbClient.Instance.ExecuteRemoteCommand("twrp wipe data", device, receiver)
                                                       writetolog($"Gaining Root Access")
                                                       AdbClient.Instance.ExecuteRemoteCommand("root", device, receiver)
                                                       AdbClient.Instance.ExecuteRemoteCommand("remount", device, receiver)
                                                       writetolog($"Installing ROM")
                                                       AdbClient.Instance.ExecuteRemoteCommand($"twrp install {input_vfile}", device, receiver)
                                                       writetolog($"Installing SuperSU")
                                                       AdbClient.Instance.ExecuteRemoteCommand($"twrp install {input_SUfile}", device, receiver)
                                                       writetolog($"Complete, rebooting")
                                                       AdbClient.Instance.ExecuteRemoteCommand("reboot", device, receiver)
                                                   Else
                                                       writetolog($"Failed, no device detected")
                                                   End If
                                               End Sub
                    instructions_flp.Controls.Add(MyButton)
            End Select
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If checkforadb() Then
            connecteddevice_lbl.Text = AdbClient.Instance.GetDevices().First.Name & $" ({AdbClient.Instance.GetDevices().First.Model})"
            connectionindicator_pb.BackColor = Color.Green
            'writetolog($"The device {AdbClient.Instance.GetDevices().First.Name} has connected to this PC")
        ElseIf checkforadbsideload() Then
            connecteddevice_lbl.Text = $"Device in Sideload Mode Attached"
            connectionindicator_pb.BackColor = Color.Yellow
            'writetolog($"Device in EDL Mode has connected to this PC")
            'TODO: readd the below
            'ElseIf checkfordevicenondebug() Then
            '    'writetolog($"Device connected without usb debugging enabled")
            '    connecteddevice_lbl.Text = "Enable USB Debugging"
            '    connectionindicator_pb.BackColor = Color.Red
        ElseIf checkforedl() Then
            connecteddevice_lbl.Text = $"Device in EDL Mode Attached"
            connectionindicator_pb.BackColor = Color.Yellow
            'writetolog($"Device in EDL Mode has connected to this PC")
            'TODO: readd the below
            'ElseIf checkfordevicenondebug() Then
            '    'writetolog($"Device connected without usb debugging enabled")
            '    connecteddevice_lbl.Text = "Enable USB Debugging"
            '    connectionindicator_pb.BackColor = Color.Red
        Else
            'writetolog($"The device {connecteddevice_lbl.Text} has disconnected from this PC")
            connecteddevice_lbl.Text = "No Device Detected"
            connectionindicator_pb.BackColor = Color.Black
        End If
        'If connectionindicator_pb.BackColor <> Color.Green AndAlso AdbClient.Instance.GetDevices().Count > 0 Then
        '    connecteddevice_lbl.Text = AdbClient.Instance.GetDevices().First.Name & $" ({AdbClient.Instance.GetDevices().First.Model})"
        '    connectionindicator_pb.BackColor = Color.Green
        '    writetolog($"The device {AdbClient.Instance.GetDevices().First.Name} has connected to this PC")
        'ElseIf connectionindicator_pb.BackColor <> Color.Yellow AndAlso checkforedl() Then
        '    connecteddevice_lbl.Text = $"Device in EDL Mode Attached"
        '    connectionindicator_pb.BackColor = Color.Yellow
        '    writetolog($"Device in EDL Mode has connected to this PC")
        'ElseIf connectionindicator_pb.BackColor <> Color.Black AndAlso AdbClient.Instance.GetDevices().Count <= 0 AndAlso Not checkforedl() Then
        '    writetolog($"The device {connecteddevice_lbl.Text} has disconnected from this PC")
        '    connecteddevice_lbl.Text = "No Device Detected"
        '    connectionindicator_pb.BackColor = Color.Black
        'ElseIf connectionindicator_pb.BackColor <> Color.Red AndAlso AdbClient.Instance.GetDevices().Count <= 0 AndAlso Not checkforedl() Then
        '    writetolog($"The device {connecteddevice_lbl.Text} has disconnected from this PC")
        '    connecteddevice_lbl.Text = "No Device Detected"
        '    connectionindicator_pb.BackColor = Color.Black
        'End If
    End Sub
    Private Function checkforadbsideload() As Boolean
        Dim alldevices = UsbDevice.AllDevices
        For Each device As UsbRegistry In alldevices
            'Console.WriteLine(device.Name)
            If device.Name.Contains("ADB") AndAlso device.DeviceProperties("FriendlyName") = "K88" Then
                Return True
                'Dim kdkd = 0
            End If
        Next
        Return False
    End Function
    Private Function checkforadb() As Boolean
        Dim alldevices = UsbDevice.AllDevices
        For Each device As UsbRegistry In alldevices
            'Console.WriteLine(device.Name)
            If device.Name.Contains("ADB") AndAlso device.DeviceProperties("FriendlyName") = "ADB Interface" Then
                Return True
                'Dim kdkd = 0
            End If
        Next
        Return False
    End Function
    Private Function checkforedl() As Boolean
        Dim alldevices = UsbDevice.AllDevices
        For Each device As UsbRegistry In alldevices
            'Console.WriteLine(device.Name)
            If device.Name = "QHSUSB__BULK" Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Function checkfordevicenondebug() As Boolean
        'LibUsbDotNet.UsbDevice.ForceLibUsbWinBack = True
        Dim alldevices = UsbDevice.AllWinUsbDevices
        Console.WriteLine(alldevices.Count)
        For Each device As UsbRegistry In alldevices
            If device.Name = "ZTE USB Device" Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Sub jasmine_BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles jasmine_BackgroundWorker1.DoWork
        Thread.Sleep(10000)
        Dim proc = New Process
        Dim startinfo = New ProcessStartInfo
        startinfo.FileName = Environment.CurrentDirectory & "\Jasmine\jasminetool-win64.exe"
        startinfo.Arguments = $"write recovery {Environment.CurrentDirectory}\Jasmine\jasmine-twrp-7.1-r3.img"
        startinfo.UseShellExecute = False
        startinfo.RedirectStandardOutput = True
        startinfo.RedirectStandardError = True
        startinfo.RedirectStandardInput = True
        startinfo.CreateNoWindow = True
        proc.StartInfo = startinfo
        proc.Start()
        While Not proc.StandardOutput.EndOfStream
            Dim thisstr = proc.StandardOutput.ReadLine
            jasmine_BackgroundWorker1.ReportProgress(0, thisstr)
            proc.StandardInput.WriteLine("I AGREE")
        End While
    End Sub

    Private Sub jasmine_BackgroundWorker1_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles jasmine_BackgroundWorker1.ProgressChanged
        Dim line As String = e.UserState.ToString()
        If line.Contains("""I AGREE""") Then
            'proc.StandardInput.WriteLine("I AGREE")
            writetolog($"TOOLKIT: Agreeing to Jasmine's disclaimer automatically")
        ElseIf line.Contains("Success!") Then
            'writetolog($"Flash completed, will reboot in 5 seconds")
        Else
            writetolog($"{line}")
        End If

    End Sub

    Private Sub jasmine_BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles jasmine_BackgroundWorker1.RunWorkerCompleted
        writetolog($"***JASMINE COMPLETED***")
    End Sub

    Private Sub openfile_sfw_pb_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub openfile_cfw_pb_Click(sender As Object, e As EventArgs)

    End Sub
End Class
