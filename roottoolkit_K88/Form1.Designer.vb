﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.connectionindicator_pb = New System.Windows.Forms.PictureBox()
        Me.connecteddevice_lbl = New System.Windows.Forms.Label()
        Me.log_tb = New System.Windows.Forms.TextBox()
        Me.steps_lb = New System.Windows.Forms.ListBox()
        Me.instructions_tb = New System.Windows.Forms.TextBox()
        Me.instructions_flp = New System.Windows.Forms.FlowLayoutPanel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.jasmine_BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.connectionindicator_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.instructions_flp.SuspendLayout()
        Me.SuspendLayout()
        '
        'connectionindicator_pb
        '
        Me.connectionindicator_pb.BackColor = System.Drawing.Color.Red
        Me.connectionindicator_pb.Location = New System.Drawing.Point(12, 420)
        Me.connectionindicator_pb.Name = "connectionindicator_pb"
        Me.connectionindicator_pb.Size = New System.Drawing.Size(30, 30)
        Me.connectionindicator_pb.TabIndex = 0
        Me.connectionindicator_pb.TabStop = False
        '
        'connecteddevice_lbl
        '
        Me.connecteddevice_lbl.AutoSize = True
        Me.connecteddevice_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.connecteddevice_lbl.Location = New System.Drawing.Point(48, 424)
        Me.connecteddevice_lbl.Name = "connecteddevice_lbl"
        Me.connecteddevice_lbl.Size = New System.Drawing.Size(206, 26)
        Me.connecteddevice_lbl.TabIndex = 1
        Me.connecteddevice_lbl.Text = "No Device Detected"
        '
        'log_tb
        '
        Me.log_tb.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.log_tb.Location = New System.Drawing.Point(338, 350)
        Me.log_tb.Multiline = True
        Me.log_tb.Name = "log_tb"
        Me.log_tb.ReadOnly = True
        Me.log_tb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.log_tb.Size = New System.Drawing.Size(392, 100)
        Me.log_tb.TabIndex = 2
        '
        'steps_lb
        '
        Me.steps_lb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.steps_lb.FormattingEnabled = True
        Me.steps_lb.ItemHeight = 20
        Me.steps_lb.Location = New System.Drawing.Point(12, 33)
        Me.steps_lb.Name = "steps_lb"
        Me.steps_lb.Size = New System.Drawing.Size(299, 244)
        Me.steps_lb.TabIndex = 3
        '
        'instructions_tb
        '
        Me.instructions_tb.BackColor = System.Drawing.SystemColors.Control
        Me.instructions_tb.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.instructions_tb.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.instructions_tb.Location = New System.Drawing.Point(3, 3)
        Me.instructions_tb.Multiline = True
        Me.instructions_tb.Name = "instructions_tb"
        Me.instructions_tb.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.instructions_tb.Size = New System.Drawing.Size(379, 241)
        Me.instructions_tb.TabIndex = 0
        '
        'instructions_flp
        '
        Me.instructions_flp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.instructions_flp.Controls.Add(Me.instructions_tb)
        Me.instructions_flp.Location = New System.Drawing.Point(338, 33)
        Me.instructions_flp.Name = "instructions_flp"
        Me.instructions_flp.Size = New System.Drawing.Size(392, 292)
        Me.instructions_flp.TabIndex = 4
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "zip"
        Me.OpenFileDialog1.InitialDirectory = "c:\"
        '
        'jasmine_BackgroundWorker1
        '
        Me.jasmine_BackgroundWorker1.WorkerReportsProgress = True
        Me.jasmine_BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(49, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 24)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Select a step below"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(766, 470)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.instructions_flp)
        Me.Controls.Add(Me.steps_lb)
        Me.Controls.Add(Me.log_tb)
        Me.Controls.Add(Me.connecteddevice_lbl)
        Me.Controls.Add(Me.connectionindicator_pb)
        Me.MinimumSize = New System.Drawing.Size(782, 497)
        Me.Name = "Form1"
        Me.Text = "Root Toolkit - ZTE Trek 2 (K88) v1.0.0.3"
        CType(Me.connectionindicator_pb, System.ComponentModel.ISupportInitialize).EndInit()
        Me.instructions_flp.ResumeLayout(False)
        Me.instructions_flp.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents connectionindicator_pb As PictureBox
    Friend WithEvents connecteddevice_lbl As Label
    Friend WithEvents log_tb As TextBox
    Friend WithEvents steps_lb As ListBox
    Friend WithEvents instructions_tb As TextBox
    Friend WithEvents instructions_flp As FlowLayoutPanel
    Friend WithEvents Timer1 As Timer
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents jasmine_BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Label1 As Label
End Class
